import java.util.Scanner;
import java.util.*;
import java.lang.*;


/*
Формула расчета ежемесячного платежа по ипотеке:
M = P [{r(1+r)^n}/{(1+r)^n – 1}]
где,
M = Ежемесячный платеж
P = сумма кредита
r = ставка по ипотеке
n = количество платежей
*/

class Payment {
    double m;

    Payment(double p, double r, double t) {
        m = (p * r) / (1 - Math.pow(1 + r, -t));
        m = Math.round(m * 100) / 100;
    }
}

class Calculator {


    public static void main(String args[]) {

        double a;
        double b;
        double c;
        double income;


        Scanner scan = new Scanner(System.in);

        System.out.print("Введите ваше ФИО : ");
        String name = scan.nextLine();

        while (name.length() == 0 || name.isEmpty()) {
            System.out.println("Отсутствует ввод ФИО повторите ввод");
            name = scan.nextLine();
            int index = -1;
            for (int i = 0; i < name.length(); i++) {
                if (Character.isDigit(name.charAt(i))) {
                    index = i; 
                    break;
                }
            }
            if (index >= 0) {
                System.out.println("Введены некорректные данные ФИО");
            }
        }
        System.out.println("----------------------------------------------------");

        System.out.print("Введите Ваш возраст: ");
        int age = scan.nextInt();
        while (age < 18 || age > 100) {
            System.out.println("К сожалению, Вам не может быть одобрен кредит, так как вы не входите в возрастную категорию 18-100 лет .");
            age = scan.nextInt();
        }
        System.out.println("----------------------------------------------------");

        System.out.print("Введите Ваш ежемесячный доход в рублях: ");
        income = scan.nextDouble();

        while (income < 0) {
            System.out.println("Введена некорректная сумма, сумма не может быть отрицательной, повторите ввод");
            income = scan.nextDouble();
        }
        System.out.println("----------------------------------------------------");

        System.out.print("Введите сумму кредитования : ");
        a = scan.nextDouble();
        while (a < 0) {
            System.out.println("Введена некорректная сумма, повторите ввод");
            a = scan.nextDouble();
        }
        System.out.println("----------------------------------------------------");

        System.out.print("Введите процентную ставку 0-100% : ");
        b = scan.nextDouble();
        while (b < 0 || b > 100) {
            System.out.println("Введена некорректная процентная ставка, повторите ввод");
            b = scan.nextDouble();
        }
        System.out.println("----------------------------------------------------");
        double proc = b;

        b = (b / 100) / 12;

        System.out.print("Введите предполагаемый срок кредита, лет : ");
        c = scan.nextInt();

        while (c < 0 || c > 100) {
            System.out.println("Введены некорректная данные, повторите ввод");
            c = scan.nextInt();
        }
        System.out.println("----------------------------------------------------");

        double years = c;
        c = c * 12;
        double month = c;

        Payment pay = new Payment(a, b, c);

        if (income / 2 < pay.m) {
            System.out.println("К сожалению, " + name + " вам отказано в получении кредита. Причина - низкий доход");
        } else {
            System.out.println("Уважаемый, " + name + ", Вам одобрена ипотека, на сумму " + a + " руб.");
            System.out.println("ваш ежемесячный платеж будет составлять: " + pay.m);
            System.out.println("Процентная ставка составляет : " + proc + " % годовых.");
            System.out.println("Срок погашения кредита " + years + " лет.");
        }

    }
}
